# TrailStash Liberty [![BSD licensed](https://img.shields.io/badge/license-BSD-blue.svg)](https://github.com/maputnik/osm-liberty/blob/gh-pages/LICENSE.md)

**Note: TrailStash Liberty is the original version of [OpenTrailStash](https://gitlab.com/trailstash/openstyle)**

<img align="right" height="128" alt="Icon of map with a magnifying glass" src="logo.svg" />
A free topographic MapLibre GL basemap style made for unpaved bicycle exploration with world coverage
using readily available datasets that can also be self-hosted. TrailStash Liberty is a fork of
[OSM Liberty Topo](https://github.com/nst-guide/osm-liberty-topo), which is itself a fork of
[OSM Liberty](https://github.com/maputnik/osm-liberty), which itself is a fork of
[OSM Bright](https://github.com/mapbox/osm-bright).  It is based on the vector tile schema of
[OpenMapTiles](https://github.com/openmaptiles/openmaptiles), with contours and terrain layers from
non-OpenStreetMap sources.
<br>
<br>

**[Edit TrailStash Liberty with Maputnik](https://maputnik.github.io/editor/?style=https://trailstash.gitlab.io/liberty/style.json)**

## Overview

The TrailStash Liberty style features the following:

-   Emphasis on unpaved trails & roads
-   Emphasis on bike-related POI such as bike shops, bikeshare stations, & convenience stores
-   Contours
-   Hillshade
-   Support for the [TrailStash OpenMapTiles fork](https://github.com/TrailStash/planetiler-openmaptiles)

## Legend

### Trails & Paths

Trails and Paths are defined as `class == path` in the
[OpenMapTiles Schema](https://openmaptiles.org/schema/) and as
`highway in path,pedestrian,cycleway,footway,bridleway` in OSM.

bike access is indicated by varying the casing by the value of the `bicycle` tag as follows:
|preview|description|tags|
|-|-|-|
|red|bike use is forbidden|`bicycle=no`|
|yellow|bike use is allowed|`bicycle=yes`|
|green|designated for bike use|`bicycle=designated`|

as such, trails and paths in the bike style are categorized as follows:
|preview|description|example|picture|
|-|-|-|-|
|![](./legend/greenblacksolid.png)|Unpaved Bike Path|[C&O Canal Towpath](https://open.trailsta.sh/#15/39.18143/-77.49154)|-|
|![](./legend/greenwhitesolid.png)|Paved Bike Path|[Virginia Capital Trail](http://open.trailsta.sh/#17.03/37.435417/-77.329046)|-|
|![](./legend/greenblackdash.png)|Mountain Bike Trail|[Fountain Head MTB Trails](http://localhost:8000/#17.42/38.718184/-77.326046)|-|
|![](./legend/yellowblackdash.png)|Unpaved Multi-Use Trail|[Rosaryville State Park Trails](https://open.trailsta.sh/#16.12/38.780601/-76.802276)|-|
|![](./legend/yellowwhitedash.png)|Paved Multi-Use Path|[Monroe Park](https://open.trailsta.sh/#18.27/37.546885/-77.450531), Sidewalks, College Campus Paths, etc|-|
|![](./legend/redblackdash.png)|Unpaved Trail Forbidding Bikes|[National Park Trails](https://open.trailsta.sh/#14.77/35.65447/-83.54698)|-|
|![](./legend/redwhitedash.png)|Paved Path Forbiding Bikes|[Virginia Ave SE sidewalk adjacent to the bike path](https://open.trailsta.sh/#18.08/38.878639/-76.996522)|-|

### Notes

#### Assumed surface when `surface` not present

Most roads, trails, and paths are assumed to be paved if the `surface` tag is not present with the
exception of `highway=path` and `highway=track`.

#### Assumed value of `bicycle`

If not present, `bicycle` is assumed to be `yes` and if `highway=cycleway`, `bicycle` is assumed to be `designated`
regardless of it's actual value.

#### Use of `mtb:scale`

the `mtb:scale` tag is used to imply `highway=path` and `surface=unpaved` regardless of their
actual values.

#### Use of `bicycle=designated` and `surface=paved`

All bike designated paved paths are treated as cycleways.

#### Tag simplification

The descriptions in OSM terms in this document are subject to the simplifications OpenMapTiles applies.
EG: [`surface` values are reduced to `paved` or `unpaved`](https://openmaptiles.org/schema/#surface)

## Differences with OSM Liberty Topo

-   Countours use [Maptiler Contours](https://cloud.maptiler.com/tiles/contours/)
    -   :+1: World Coverage
    -   :-1: Only have tiles in meters, so rendered in feet(only done in the USA) they seem random
-   Uses standard OpenMapTiles
    -   :+1: World Coverage
    -   :-1: Trails not visible as early as on OSM Liberty Topo
    -   :-1: Missing extra OSM Liberty Topo POI
-   Puts more focus on unpaved trails & roads using the OpenMapTiles `surface` property
-   Certain POI (bike shops, bikeshare stations, & convenience stores) are made more prominent
-   `landuse=stadium|pitch|track|quarry` rendered
-   `landcover=wetland` rendered


## Data Sources

-   [OpenMapTiles](http://openmaptiles.org/) as vector data source
-   [Natural Earth Tiles](https://klokantech.github.io/naturalearthtiles/) for low-zoom relief shading
-   icons:
    -   [Maki](https://www.mapbox.com/maki-icons/)
        -   most ions
    -   [Temaki](https://github.com/ideditor/temaki)
        -   icons not in maki
        -   bicycle rental intea of bike from maki
    -   [NPS Symbol Library](https://www.nps.gov/maps/tools/symbol-library/index.html)
        -   deer used for WMAs
-   [Contours](https://cloud.maptiler.com/tiles/contours/) come from MapTiler
-   Hillshade layer, using public [AWS Terrain Tiles](https://registry.opendata.aws/terrain-tiles/)

## Map Design

The map design originates from OSM Liberty/LibertyTopo/Bright but strives to to emphasize unpaved
trails and roads by rendering them in a darker color and colors casing by bicycle access.

## Contributing

The style is developed using [charities](https://github.com/unvt/charites).

## Icon Design

A [Maki](https://github.com/mapbox/maki) icon set using colors to distinguish between icon categories.

**Color Palette**

| Color Name | Hex Value |
| ---------- | --------- |
| Blue       | `#5d60be` |
| Light Blue | `#4898ff` |
| Orange     | `#d97200` |
| Red        | `#ba3827` |
| Brown      | `#725a50` |
| Green      | `#76a723` |

**Modify Icons**

1. Take the `sprite-src/iconset/iconset-trailstash.json` and import it to the [Maki Editor](https://www.mapbox.com/maki-icons/editor/).
2. Apply your changes and download the icons in SVG format and the iconset in JSON format.
3. replace the contents of `sprite-src/iconset` with the contents of the zip download from the Maki
   Editor.
4. run `make`

**Note:** sprite zero seems to require node version 6.17.1 https://github.com/mapbox/spritezero/issues/84#issuecomment-656327476
