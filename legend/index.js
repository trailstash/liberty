const emptyStyle = { version: 8, sources: {}, layers: [] };

function init() {

  var map = new maplibregl.Map({
    container: "map",
    style: emptyStyle,
    zoom: 18,
    center: [0, 0],
    transformRequest: (url, resourceType) => {
      if (url.includes("{key}")) {
        return { url: url.replace("{key}", "ynkLG0cZJRr8pf4pHtl1") };
      }
    },
    hash: true,
  });

  // add controls
  map.addControl(new maplibregl.NavigationControl());

  // register service worker for PWA
  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("/sw.js").then(function (registration) {
      registration.update();
      console.log("Service Worker Registered");
    });
  }

  // function to fetch & set style
  async function setStyle(layerId) {
    var styleFilename = "../style.json";
    var dataFilename = "./simple.geojson";
    if (layerId == "bike") {
      styleFilename = "../style-bike.json"
      dataFilename = "./bike.geojson"
    } else if (layerId == "full") {
      styleFilename = "../style-bike.json"
      dataFilename = "./full.geojson"
    }
    let resp = await fetch(styleFilename)
    const style = await resp.json()
    resp = await fetch(dataFilename);
    data = await resp.json();
    const layers = [{
      "id": "background",
      "type": "background",
      "layout": {"visibility": "visible"},
      "paint": {
        "background-color": "hsla(98, 61%, 72%, 0.7)"
      }
    }];
    for (const layer of style.layers) {
      if (layer.source == 'openmaptiles' && ['transportation', 'transportation_name'].includes(layer['source-layer'])){
        delete layer['source-layer']
        layers.push(layer)
      }
    }
    delete style.sources.opentrailstash;
    style.layers = layers;
    style.sources.openmaptiles = {type: "geojson", data};

    // Create a 'LngLatBounds' with both corners at the first coordinate of the first line.
    const bounds = new maplibregl.LngLatBounds(
      data.features[0].geometry.coordinates[0],
      data.features[0].geometry.coordinates[0]
    );
    for (const feature of data.features) {
      // Geographic coordinates of the LineString
      const coordinates = feature.geometry.coordinates;
      // Extend the 'LngLatBounds' to include every coordinate in the bounds result.
      for (const coord of coordinates) {
        bounds.extend(coord);
      }
    }
    map.fitBounds(bounds, {
      padding: 20
    });
    map.setStyle(style);
  }

  document.querySelector("#menu select").onchange = function switchLayer(
    event
  ) {
    var layerId = event.target.value;
    localStorage.setItem("legendstyle", layerId);
    setStyle(layerId);
  };

  // set style
  var style = localStorage.getItem("legendstyle") || "simple";
  document.querySelector("#menu select").value = style;
  setStyle(style);
}

init();
