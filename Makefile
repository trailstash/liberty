SHELL := /bin/bash
PORT := 8000

style.json: style.yml layers Makefile # Build the style
	charites build style.yml style.json
	sed -i'' -e "s/{key}/$${MAPTILER_KEY}/g" style.json

public: style.json # Build gitlab pages public folder
	mkdir -p public
	cp -r style.json public/. 

clean: # Clean up compiled style & GitLab Pages build
	rm -fr public style.json

# sprites requires old version of node for use with spritezero, namely 6.17.1
sprites: sprites/opentrailstash.png sprites/opentrailstash@2x.png # Build spritesheet
sprites/opentrailstash.png: sprite-src/allsvgs
	bash -c "cd sprites && spritezero opentrailstash ../sprite-src/allsvgs"
sprites/opentrailstash@2x.png: sprite-src/allsvgs
	bash -c "cd sprites && spritezero --retina opentrailstash@2x ../sprite-src/allsvgs"
sprite-src/allsvgs: sprite-src/iconset/svgs sprite-src/extra_svgs
	mkdir -p sprite-src/allsvgs
	cp sprite-src/iconset/svgs/* sprite-src/allsvgs/
	cp sprite-src/extra_svgs/* sprite-src/allsvgs/

.PHONY: all sprites sprite-src/allsvgs help clean serve
help: # Print help
	@awk 'BEGIN {FS = ":.*?# *"} /^[.a-zA-Z_-]+:.*?# */ {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
